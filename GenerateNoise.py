
from __future__ import print_function
import numpy as np
import sys, os
import argparse
import subprocess


class NoiseGenerate(object):

    def __init__(self, tobs, tsamp, mjd, fch1, chbw, nbits, nchans, name, output, seed=False):

        self.tobs            = tobs       # Observation length (s)
        self.tsamp           = tsamp      # Sampling time (us)
        self.mjd             = mjd        # MJD of first sample
        self.fch1            = fch1       # Frequency of channel 1 (MHz)
        self.chbw            = chbw       # Channel bandwidth (MHz)
        self.nbits           = nbits      # Number of bits
        self.nchans          = nchans     # Number of channels
        self.name            = name       # Source name
        self.output          = output     # Output filename
        self.seed            = seed       # Random seed


    def form_command(self):
        '''
        Sets up the call to fast_fake
        '''
        if not self.seed:
            command = ['fast_fake', 
                       '-T', str(self.tobs),
                       '-t', str(self.tsamp),
                       '-m', str(self.mjd),
                       '-F', str(self.fch1),
                       '-f', str(self.chbw),
                       '-b', str(self.nbits),
                       '-c', str(self.nchans),
                       '-s', str(self.name),
                       '-o', str(self.output)]
        else: 
            command = ['fast_fake', 
                       '-T', str(self.tobs),
                       '-t', str(self.tsamp),
                       '-m', str(self.mjd),
                       '-F', str(self.fch1),
                       '-f', str(self.chbw),
                       '-b', str(self.nbits),
                       '-c', str(self.nchans),
                       '-s', str(self.name),
                       '-S', str(self.seed),
                       '-o', str(self.output)]
        return command


    def print_command(self):
        self.com = self.form_command()

        # Call fast_fake
        running_now = ' '.join(map(str, self.com)) 
        print("Running:\n {}".format(running_now), file=sys.stderr)

        return None


    def run_command(self):
        self.com = self.form_command()

        proc = subprocess.Popen(self.com, stdout=subprocess.PIPE)
        proc.communicate()
        proc.wait()



def main(argv=None):

    # Process command line args
    parser = argparse.ArgumentParser(description='Test vector noise file generator')
    parser.add_argument('-T','--tobs', help='Integration time (s)', required=False, type=float, default=600.0)
    parser.add_argument('-t','--tsamp', help='Samplig time (us)', required=False, type=float, default=64.0)
    parser.add_argument('-m','--mjd', help='MJD of first sample', required=False, type=float, default=56000.0)
    parser.add_argument('-F','--fch1', help='Frequency of channel 1 (MHz)', required=False, type=float, default=1670.0)
    parser.add_argument('-f','--chbw', help='Channel bandwidth (MHz)', required=False, type=float, default=-0.078125)
    parser.add_argument('-b','--nbits', help='Number of bits', required=False, type=int, default=8)
    parser.add_argument('-c','--nchans', help='Number of channels', required=False, type=int, default=4096)
    parser.add_argument('-s','--name', help='Source name', required=False, type=str, default='noise')
    parser.add_argument('-o','--output', help='Output filename', required=False, type=str, default='noise.fil')
    parser.add_argument('-S','--seed', help='Random seed', required=False, type=int)

    args = parser.parse_args()

    generate = NoiseGenerate(args.tobs, args.tsamp, args.mjd, args.fch1, args.chbw, args.nbits, args.nchans, args.name, args.output, args.seed)
    
    generate.print_command()
    generate.run_command()


if __name__ == '__main__':
    main()
